$(call inherit-product, device/samsung/m20lte/full_m20lte.mk)

# Inherit some common ArrowOS stuff.
$(call inherit-product, vendor/arrow/config/common.mk)

PRODUCT_NAME := arrow_m20lte
TARGET_FACE_UNLOCK_SUPPORTED := true
