PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/arrow_m20lte.mk

COMMON_LUNCH_CHOICES := \
    arrow_m20lte-userdebug \
    arrow_m20lte-user \
    arrow_m20lte-eng
